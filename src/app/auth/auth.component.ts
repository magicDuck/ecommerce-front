import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import {User} from '../entity/User';
import {RestResponse} from '../entity/RestResponse'
import {Router} from '@angular/router';

import {AuthService} from '../services/auth/auth.service';
import { Client } from '../entity/Client';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  emailFormControl: FormControl;
  password: String;
  hide: boolean;
  restResponse: RestResponse;
  user: User;
  userExist: boolean
  
  constructor(private router: Router,private authService: AuthService) {

    this.emailFormControl = new FormControl('', [
      Validators.required,
      Validators.email,
    ]);

    this.password = '';
    this.hide = true;
    this.userExist = true;
  }

  onKey(event: KeyboardEvent) { 
    this.password = (event.target as HTMLInputElement).value ;
  }

  isValid(){

    return this.emailFormControl.valid && this.password.length> 0 ? true: false;
  }

  validate() {

    this.login().then(result =>{
      this.restResponse = <RestResponse>result;
      
      if(this.restResponse.response === 302){
        this.user = <User>this.restResponse.message;
        this.authService.saveUser(this.user);
        if(this.user.userType === 'CLIENT'){
          this.router.navigate(["/purchase/:email",{email: this.user.email}]);
        }
        if(this.user.userType === 'ADMIN'){
          this.router.navigate(["/admin/:email",{email: this.user.email}]);
        }
      }
      else{
        this.userExist = false;
      }
    });
    
  }

  login(){

    let client: Client;
    let user: User= {id: 0, email: this.emailFormControl.value, password: this.password, userType: "",idClient: 0,clientDTO: client}

    return new Promise((result) => {
      this.authService.loggin(user).subscribe(
        response => {
          result(response );
        });
      });
  }

  clear(){

    this.emailFormControl.setValue('');
    console.log("aca deberia limpiar todo");

  }

  ngOnInit(): void {
  }

}
