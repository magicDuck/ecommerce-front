import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class AdminService {

  private url = 'http://localhost:8080/admin';
  constructor(private httpClient: HttpClient) { }

  getCurrentVIPs(): Observable<any>
  {
    return this.httpClient.get(this.url+"/get/current/VIPs");
  }

  getVIPsOf(date: String): Observable<any>
  {
    return this.httpClient.post(this.url+"/get/VIPs/for/month",date);
  }

  getVIPsEndedOf(date: String): Observable<any>
  {
    return this.httpClient.post(this.url+"/get/VIPs/for/end/month",date);
  }
}
