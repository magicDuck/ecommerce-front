import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from 'src/app/entity/User';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url = 'http://localhost:8080/auth/login';

  private isLoggedIn: boolean;
  private user: User;

  constructor(private httpClient: HttpClient) {
    this.isLoggedIn = false;
   }

  saveUser(user: User){
    this.isLoggedIn = true;
    this.user = user;
  }

  logout(){
    this.isLoggedIn = false;
    this.user = null;
  }

  isUserLoggedIn(): boolean {
    return this.isLoggedIn;
  }

  getUserSaved(){
    return this.user;
  }

  getUserSavedName(){
    return this.user.clientDTO.name;
  }

  getUserSavedLastName(){
    return this.user.clientDTO.lastName;
  }

  loggin(user: User): Observable<any>
  {
    return this.httpClient.post(this.url,user);
  }

  logoutUser(): void{
    this.isLoggedIn = false;
  }

}
