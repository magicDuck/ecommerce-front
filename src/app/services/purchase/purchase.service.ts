import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from 'src/app/entity/Client';
import { User } from 'src/app/entity/User';
import { ShoppingCart } from 'src/app/entity/ShoppingCart';

@Injectable({
  providedIn: 'root'
})
export class PurchaseService {

  private urlCart = 'http://localhost:8080/shoppingCart';

  private urlItems ='http://localhost:8080/item';

  constructor(private httpClient: HttpClient) { }


  getShoppingCart(user: User): Observable<any>
  {
    console.log(user)
    return this.httpClient.post(this.urlCart+"/get/by/client",user);
  }

  getItems(): Observable<any>
  {
    return this.httpClient.get(this.urlItems+"/get/all/by/available");
  }

  getPriceFromServer(shoppingCart: ShoppingCart): Observable<any>
  {
    return this.httpClient.post(this.urlCart+"/get/price/from/cart",shoppingCart);
  }

  saveShoppingCart(shoppingCart: ShoppingCart): Observable<any>
  {
    return this.httpClient.post(this.urlCart+"/save/shoppingCart",shoppingCart);
  }

}
