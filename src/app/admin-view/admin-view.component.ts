import { Component, OnInit } from '@angular/core';
import {User} from '../entity/User';
import {FormControl} from '@angular/forms';
import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatDatepicker} from '@angular/material/datepicker';
import {AdminService} from '../services/admin/admin.service';

import * as _moment from 'moment';
import {default as _rollupMoment, Moment} from 'moment';
import { Client } from '../entity/Client';
import { RestResponse } from '../entity/RestResponse'
import { AuthService } from '../services/auth/auth.service';
import {Router} from '@angular/router';

interface OptionSelect {
  value: string;
  viewValue: string;
}

const moment = _rollupMoment || _moment;
export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY-MM',
  },
  display: {
    dateInput: 'YYYY-MM',
    monthYearLabel: 'YYYY-MMM',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY-MMMMM',
  },
};

@Component({
  selector: 'app-admin-view',
  templateUrl: './admin-view.component.html',
  styleUrls: ['./admin-view.component.css'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class AdminViewComponent implements OnInit {

  options: OptionSelect[] = [
    {value: '1', viewValue: 'VIPs del mes actual'},
    {value: '2', viewValue: 'VIPs de un mes en especifico'},
    {value: '3', viewValue: 'VIPs que dejaron de serlo en un mes especifico'}
  ];
  optionSelected: String;

  clients: Client[] = [];
  displayedColumns: string[] = ['name', 'lastname', 'startdate', 'enddate']
  date = new FormControl(moment());
  endDate = new FormControl(moment());
  clientName: String;
  clientLastName: String;
  
  constructor(private adminService: AdminService, private authService: AuthService, private router: Router) {
  
    this.clientName = this.authService.getUserSavedName();
    this.clientLastName = this.authService.getUserSavedLastName();
   }

  ngOnInit(): void {
  }


  logout(){
    this.authService.logout();
    this.router.navigate([""]);
  }

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenEndYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.endDate.value;
    ctrlValue.year(normalizedYear.year());
    this.endDate.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
  }

  chosenEndMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.endDate.value;
    ctrlValue.month(normalizedMonth.month());
    this.endDate.setValue(ctrlValue);
    datepicker.close();
  }


  capture(data){
    if(data ==='1' ){
      this.getCurrentVIPs();
    }
  }

  searchForStartDate(){
    console.log("ESTOY EN 2");
    let dateSelected = moment(this.date.value).format('YYYY-MM')+"-01";
    
    this.clear()
    this.searchVIPsForMonth(dateSelected);
  }

  searchForEndDate(){
    console.log("ESTOY EN 3");
    let dateSelected = moment(this.endDate.value).format('YYYY-MM')+"-01";

    this.clear()
    this.searchVIPsForEndMonth(dateSelected);
  }

  clear(){
    this.clients=[]
  }

  getCurrentVIPs(){
    this.getCurrentVIPsFromServer().then(result => {
      let restResponse = <RestResponse>result;
      console.log(restResponse)
      if (restResponse.response === 302) {
        this.clients = <Client[]> restResponse.message;
      }

    });

  }

  getCurrentVIPsFromServer(){
    return new Promise((result) => {      
      this.adminService.getCurrentVIPs().subscribe(
        response => {
          result(response);
        });
    });
  }

  searchVIPsForMonth(date: String){
    this.searchVIPsForMonthFromServer(date).then(result => {
      let restResponse = <RestResponse>result;
      console.log(restResponse)
      if (restResponse.response === 302) {
        this.clients = <Client[]> restResponse.message;
      }
    });
  }

  searchVIPsForMonthFromServer(date: String){
    return new Promise((result) => {      
      this.adminService.getVIPsOf(date).subscribe(
        response => {
          result(response);
        });
    });
  }

  searchVIPsForEndMonth(date: String){
    this.searchVIPsForEndMonthFromServer(date).then(result => {
      let restResponse = <RestResponse>result;
      console.log(restResponse)
      if (restResponse.response === 302) {
        this.clients = <Client[]> restResponse.message;
      }
    });
  }

  searchVIPsForEndMonthFromServer(date: String){
    return new Promise((result) => {      
      this.adminService.getVIPsEndedOf(date).subscribe(
        response => {
          result(response);
        });
    });
  }
}
