import { Component, OnInit } from '@angular/core';
import { Item } from '../entity/item';
import { PurchaseService } from '../services/purchase/purchase.service';
import { AuthService } from '../services/auth/auth.service';
import { RestResponse } from '../entity/RestResponse'
import { ShoppingCart } from '../entity/ShoppingCart';
import {Router} from '@angular/router';

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.css']
})
export class PurchaseComponent implements OnInit {

  dataSource: Item[] = [];
  itemToBuy: Item[] = [];
  message: String = '';
  price : String = '';
  isEditable = true;
  restResponse: RestResponse;
  shoppingCart: ShoppingCart;
  displayedColumns: string[] = ['name', 'price', 'quantity'];

  clientName: String;
  clientLastName: String;

  constructor(private router: Router, private purchaseService: PurchaseService, private authService: AuthService) {

    this.searchCart();
    this.searchItems()

    this.clientName = this.authService.getUserSavedName();
    this.clientLastName = this.authService.getUserSavedLastName();
    
  }

  ngOnInit() {
    
  }

  send(stepper){

    this.saveShoppingCart(stepper);
  }

  logout(){
    this.authService.logout();
    this.router.navigate([""]);
  }

  clear(stepper){
    for (let item of this.dataSource) {
      item.quantity= 0;
    }
    this.itemToBuy =[];
    this.price = '';
    stepper.reset();
    this.searchItems();
  }

  getDate(){
    let date = new Date()
    let month = date.getMonth() + 1
    let day = date.getDay();
    let monthString = "";
    let dayString= "";
    if(month < 10){

      monthString = "0"+String(month);
    }else{
      monthString= String(month);
     
    }

    if(day < 10){

      dayString = "0"+String(day);
    }else{
      dayString= String(day);
     
    }
    return String(date.getFullYear())+"-"+monthString+"-"+dayString;
  }

  saveShoppingCart(stepper){
    this.saveShoppingCartInServer().then( result => {
      this.restResponse = <RestResponse>result;
      if (this.restResponse.response !== 202) {
        alert("Ocurrio un error y no se guardo su carrito");
        this.searchCart();
      }
      else{
        alert("Guardado correctamente");
        this.searchCart()
      }
      //limpiar pantalla?
    });

  
    this.clear(stepper);
  }

  saveShoppingCartInServer(){
    return new Promise((result) => {      
      this.purchaseService.saveShoppingCart(this.shoppingCart).subscribe(
        response => {
          result(response);
        });
    });
  }

  searchCart() {

    this.searchUserCartFromService().then(result => {
      this.restResponse = <RestResponse>result;
      if (this.restResponse.response === 302) {
        this.shoppingCart = <ShoppingCart>this.restResponse.message;
        this.message = "Se le ha asignado un carrito de tipo: "+ this.getTypeOfCart(this.shoppingCart.shoppingCartType);
      }

    });
  }

  searchUserCartFromService() {
    return new Promise((result) => {      
      this.purchaseService.getShoppingCart(this.authService.getUserSaved()).subscribe(
        response => {
          result(response);
        });
    });
  }

  searchItems() {
    this.searchItemsFromService().then((result: RestResponse) => {
      if (result.response === 302) {
        this.dataSource =<Item[]> result.message;
        
      }

    });
  }

  searchItemsFromService() {
    return new Promise((result) => {
      this.purchaseService.getItems().subscribe(
        response => {
          result(response);
        });
    });
  }

  isValid() {
    for (let item of this.dataSource) {
      if (item.quantity > 0) {
        return true;

      }
    }
    return false;
  }


  filterItems() {
    this.itemToBuy = this.dataSource.filter((item) => {
      if (item.quantity > 0) {
        return item;
      }
    });
    this.shoppingCart.items =this.itemToBuy;
    this.getPrice()
  }

  getPrice(){
    this.getPricefromServer().then((result: RestResponse) => {
      if (result.response === 100) {
        this.shoppingCart =<ShoppingCart> result.message;
        this.price = "El precio total del carrito es de : " +"$"+ String(this.shoppingCart.shoppingCartPrice)+" , para finalizar la compra haga click en confirmar (las compras no finalizadas no se guardarán)";
      }
    });

  }

  getPricefromServer(){
    return new Promise((result) => {
      this.purchaseService.getPriceFromServer(this.shoppingCart).subscribe(
        response => {
          result(response);
        });
    });
  }

  getTypeOfCart(typeCart: String) {
    switch (typeCart) {
      case ("DEFAULT"):
        return "normal";
      case ("SPECIAL_DAY"):
        return "especial. Ya que hoy es una fecha especial, recibirá un descuento del precio total al finalizar la compra solo por hoy";
      case ("VIP"):
        return "VIP por haber realizado el mes pasado compras cuya suma total fueron mayor a $10000, recibirá un descuento del precio total si compra más de 10 productos en esta oportunidad";

    }
  }

}
