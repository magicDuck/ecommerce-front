import { Item } from '../entity/item';

export interface ShoppingCart{

    id: number;
    shoppingCartType: String;
    date: String;
    shoppingCartPrice: number
    idUser: number
    items: Item[];
}