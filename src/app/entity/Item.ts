export interface Item{

    id: number;
    name: String;
    price: String;
    isAvailable: number;
    quantity: number;

}