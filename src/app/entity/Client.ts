export interface Client{

    id: number;
    name: String;
    lastName: String;
    isVIP: number;
    startVIPDate: String;
    endVIPDate: String;
}