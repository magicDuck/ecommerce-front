import {Client} from './Client';

export interface User{
    id: number;
    email: String;
    password: String;
    userType: String,
    idClient: number;
    clientDTO: Client;
}