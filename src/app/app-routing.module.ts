import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { PurchaseComponent } from './purchase/purchase.component';
import {AdminViewComponent} from './admin-view/admin-view.component';
import {AuthGuard} from './guards/auth.guard';

const routes: Routes = [

  {path: '', component: AuthComponent},
  {path: 'purchase/:email', component: PurchaseComponent, canActivate: [AuthGuard]},
  {path: 'admin/:email', component: AdminViewComponent, canActivate: [AuthGuard]},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
